from __future__ import division
import pandas as pd
import numpy as np
import ast
import csv
import cPickle
import matplotlib.pyplot as plt
import ast
import collections
import math
import os
import copy

def weights(twoFirms):
    data = pd.read_csv('CPICategories.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]
    heads = list(data.columns.values)
    MyCodeWeights = collections.OrderedDict({})

    for r in xrange(rows):
        d = data.iloc[r][:]
        code = int(d['MyCode'])
        weight = d['CPI-U']
        MyCodeWeights[code] = weight


    data = pd.read_csv('CPICode.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0]
    heads = list(data.columns.values)

    myCodeBEASizes = collections.OrderedDict({})
    myCodeBEATotal = collections.OrderedDict({})
    myCodeBEAShares = collections.OrderedDict({})

    for r in xrange(rows):
        d = data.iloc[r][:]
        bea = d['Code']
        if not isinstance(bea, float):
            myCode = d['MyCode']
            pce = d['PCE']
            if myCode == '*':
                pass
            elif isinstance(myCode, str):
                if myCode not in myCodeBEASizes:
                    myCodeBEASizes[myCode] = {}
                myCodeBEASizes[myCode][bea] = pce

    for myCode in myCodeBEASizes:
        total = 0
        for k in myCodeBEASizes[myCode]:
            num = myCodeBEASizes[myCode][k]
            if not isinstance(num, float):
                num = int(num.replace(',', ''))
                total += num
        myCodeBEATotal[myCode] = total

    beaWeights = {}
    for myCode in myCodeBEASizes:
        allBEA = myCodeBEASizes[myCode]
        allSizes = myCodeBEASizes[myCode]
        for bea in allBEA:
            total = myCodeBEATotal[myCode]
            size = allSizes[bea]
            size = size.replace(",", "")
            size = int(size)
            sh = size / total
            codes = myCode.split(",")
            for c in codes:
                c = int(c)
                weight = MyCodeWeights[c]
                sh_weight = weight * sh
                if bea not in beaWeights:
                    beaWeights[bea] = sh_weight
                else:
                    beaWeights[bea] += sh_weight


    for bea in beaWeights:
        beaWeights[bea] /= 100

    tot = sum(beaWeights.values())
    for bea in beaWeights:
        beaWeights[bea] /= tot

    with open('beaCPIWeights.txt', 'w') as file:
        for bea in beaWeights:
            if twoFirms:
                beaC = bea + 'C'
            else:
                beaC = bea
            weight = beaWeights[bea]
            pair = [beaC, weight]
            pair = ','.join(map(str, pair))
            file.write(pair + '\n')

    indexWeights = {}
    indexes = {}
    with open('index.txt', 'r') as file:
        for line in file:
            line = line.split(",")
            bea = line[0]
            index = int(line[1])
            indexes[bea] = index

    for bea in beaWeights:
        if twoFirms:
            beaC = bea + 'C'
        else:
            beaC = bea
        ind = indexes[beaC]
        weight = beaWeights[bea]
        indexWeights[ind] = weight

    with open('CPI_weights.txt', 'w') as file:
        for ind in indexWeights:
            weight = indexWeights[ind]
            pair = [ind, weight]
            pair = ','.join(map(str, pair))
            file.write(pair + '\n')







weights(twoFirms=True)