cpiWeights.py uses a file containing CPI weights (constructed through judgement) to compute the CPI weights of various indexes. The indexes pertain to sectors of IO table, and are gotten from the IO table process folder.


===============================
BEAHandbook pages 11-17 has a mapping from NIPALine to which CPI Index is used to deflate PCE
PCE-CPI-Reconcliation 29-37 has mapping from PCE Index to CPI
PCEBridge(2007) has mapping from NIPA line item to PCE BEA IO Code
We need a mapping from PCE BEA IO code to PCE Index (perhaps via NIPA line?)

Write this up as a clear research question with input files, and expected output file with documentation